#!/bin/bash

# Создайте пользователей и установите пароли — user1, user2, user3
for i in {1..3}
do
  echo -e "user$i:user$i::::/home/user$i:/bin/bash" >> ~/users.txt
done
sudo chmod 600 ~/users.txt
# Check
cat ~/users.txt
sudo newusers ~/users.txt
rm ~/users.txt
# Создание групп – devops, aws
sudo groupadd devops
sudo groupadd aws
# Измените основную группу user2, user3 на группу «devops»
for i in {2..3}
do
  sudo usermod -g devops user$i
done
# Добавьте группу «aws» в качестве дополнительной группы к «user1».
sudo usermod -aG aws user1
# Check
tail -3 /etc/passwd
tail /etc/group
# Создайте структуру файлов и каталогов, показанную на схеме  
sudo mkdir -p /dir1 /dir2/dir1/dir2/dir10 /dir3/dir11 /dir4/dir12 /dir5/dir13 /dir6 /dir7/dir10 /dir8/dir9 /opt/dir14/dir10
sudo touch /dir1/f1 /dir2/dir1/dir2/f3 /dir4/dir12/f5 /dir4/dir12/f4 /dir7/f3 /f1 /f2 /opt/dir14/f3
# Измените группу /dir1, /dir7/dir10, /f2 на группу «devops»
# Измените владельца /dir1, /dir7/dir10, /f2 на пользователя «user1»
sudo chown -R user1:devops /dir1 /dir7/dir10 /f2
# Check
ls -la /dir1
ls -la /dir7
ls -la /f2

#Для выполнения 2 части задания необходимо добавить пользователю user1 права sudo и залогиниться под user1
sudo adduser user1 sudo
echo "user1 ALL=(ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers > /dev/null
sudo su -l user1