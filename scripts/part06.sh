#!/bin/bash

# Войдите в систему как пользователь root и выполните действия ниже.
# Найдите файл с именем «f3» на сервере и перечислите все абсолютные пути, по которым находится файл f3.
sudo find / -type f -name f3
# Показать подсчет количества файлов в каталоге '/'
ls -l / | grep ^- | wc -l
# Распечатайте последнюю строку файла «/etc/passwd»
tail -1 /etc/passwd