#!/bin/bash

# Войдите в систему как «user1» и выполните действия ниже.
# Создайте каталог — «/home/user2/dir1».
sudo mkdir /home/user2/dir1
echo "check mkdir /home/user2/dir1"
ls -la /home/user2
# Перейдите в каталог «/dir2/dir1/dir2/dir10» и создайте файл «/opt/dir14/dir10/f1», используя метод относительного пути.
cd /dir2/dir1/dir2/dir10
pwd
sudo touch ../../../../opt/dir14/dir10/f1
echo "check touch ../../../../opt/dir14/dir10/f1"
ls -la /opt/dir14/dir10/
# Переместите файл из «/opt/dir14/dir10/f1» в домашний каталог user1.
sudo chown user1:user1 /opt/dir14/dir10/f1
mv /opt/dir14/dir10/f1 ~/
echo "check mv /opt/dir14/dir10/f1 ~/"
ls -la ~/
# Рекурсивно удалить каталог «/dir4»
sudo rm -rf /dir4
echo "check rm -rf /dir4"
ls -la /dir*
# Удалите все дочерние файлы и каталоги в папке «/opt/dir14» с помощью одной команды.
sudo rm -rf /opt/dir14/*
echo "check rm -rf /opt/dir14/*"
ls -la /opt/dir14/
# Напишите этот текст “Linux assessment for an DevOps Engineer!! Learn with Fun!!” в файл /f3 и сохраните его.
echo 'Linux assessment for an DevOps Engineer!! Learn with Fun!!' | sudo tee -a /f3 > /dev/null
# check
sudo cat /f3

sudo adduser user2 sudo
echo "user2 ALL=(ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers > /dev/null
sudo su -l user2