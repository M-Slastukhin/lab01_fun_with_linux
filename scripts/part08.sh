#!/bin/bash

#  Войдите в систему как пользователь root и выполните действия ниже.
# Создайте файловую систему на новом томе, прикрепленном на предыдущем шаге.
sudo fdisk -l
device=$(sudo fdisk -l | grep '5 GiB' | sed -E -e 's/^Disk (.*):.*/\1/')
sudo fdisk $device
lt_device=${device}1
sudo mkfs.ext4 $lt_device
# создаем файловую систему в интерактивном режиме
# Смонтируйте файловую систему в каталоге /data.
sudo mkdir /data
sudo mount $lt_device /data
# echo "$lt_device /data ext4 defaults 0 0" | sudo tee -a /etc/fstab
# Проверьте использование файловой системы с помощью команды «df -h». Эта команда должна показать файловую систему /data.
df -h
# Создайте файл «f1» в файловой системе /data.
sudo touch /data/f1
ls /data