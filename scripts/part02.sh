#!/bin/bash


# Войдите в систему как user1 и выполните действия ниже.
# Создание пользователей и установка паролей — user4, user5.
for i in {4..5}
do
  echo -e "user$i:user$i::::/home/user$i:/bin/bash" >> ~/users.txt
done
sudo chmod 600 ~/users.txt
# check
cat ~/users.txt
sudo newusers ~/users.txt
rm ~/users.txt
# Создание групп – app, database
sudo groupadd app
sudo groupadd database
# Check
tail -3 /etc/passwd
tail /etc/group

#Для выполнения 3 части задания необходимо добавить пользователю user4 права sudo
sudo adduser user4 sudo
echo "user4 ALL=(ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers > /dev/null
sudo su -l user4