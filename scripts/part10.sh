#!/bin/bash

# 10. Войдите в систему как пользователь root и выполните действия ниже.
# Удалить пользователей – «user1, user2, user3, user4, user5».
for i in {1..5}
do
  sudo userdel -r user$i
done
# Удалить группы – app, aws, database, devops
values=(app aws database devops)
for i in "${values[@]}"
do
  sudo groupdel -f $i
done
# Удалите домашние каталоги всех пользователей «user1, user2, user3, user4, user5», если они еще существуют.
ls -la /home
# Размонтировать файловую систему /data
cd ~
sudo umount $lt_device
# Удалить каталог /data
sudo rm -rf /data
ls -la /