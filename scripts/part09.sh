#!/bin/bash

# 9. Войдите в систему как «user5» и выполните действия ниже.
# Удалить /dir1
# Удалить /dir2
# Удалить /dir3
# Удалить /dir5
# Удалить /dir7
# Удалить /f1 и /f4
# Удалить /opt/dir14
sudo adduser user5 sudo
echo "user5 ALL=(ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers > /dev/null
sudo su -l user5 -c 'sudo rm -rf /dir* /f* /opt/dir14'