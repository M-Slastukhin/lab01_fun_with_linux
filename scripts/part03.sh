#!/bin/bash

# 3. Войдите в систему как «user4» и выполните действия ниже.
# Создать каталог — /dir6/dir4
sudo mkdir /dir6/dir4
# Создать файл – /f3
sudo touch /f3
# Переместите файл из «/dir1/f1» в «/dir2/dir1/dir2».
sudo mv /dir1/f1 /dir2/dir1/dir2
# Переименуйте файл «/f2» в «/f4»
sudo mv /f2 /f4
#check
ls -la /dir6/dir4
ls -la /f*
ls -la /dir2/dir1/dir2/f1
exit