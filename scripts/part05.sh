#!/bin/bash

# Войдите в систему как «user2» и выполните действия ниже.
# Создайте файл «/dir1/f2»
sudo touch /dir1/f2
ls -la /dir1
# Удалить /dir6
# Удалить /dir8
sudo rm -rf /dir6 /dir8
# Замените текст «DevOps» на «devops» в файле /f3 без использования редактора.
sudo cat /f3
sudo sed 's/DevOps/devops/g' /f3  | sudo tee /f3
sudo cat /f3
# С помощью Vi-Editor скопируйте строку 1 и вставьте 10 раз в файл /f3.
sudo vim -c 'yy' -c '10p' -c ':wq' /f3
# в интерфейсе Vim yy10p:wq
# Найдите шаблон «Engineer» и замените его на «engineer» в файле /f3 с помощью одной команды.
# Удалить /f3
sudo rm -f /f3
sudo su -l vagrant